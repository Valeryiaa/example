import React from 'react';
import {useTitle} from "react-use";

const Page404 = props => {
  useTitle('Error')
  return <>404 </>;
};

export default Page404;
